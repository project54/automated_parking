package event_handlers

type CarArrivedEvent struct {
	CarNumber string
}

type CarArrivedHandler struct {
}

func (h *CarArrivedHandler) Handle(event CarArrivedEvent) error {
	return nil
}
